package com.hypervelocityconsulting.api;

public interface MyPluginComponent
{
    String getName();
}