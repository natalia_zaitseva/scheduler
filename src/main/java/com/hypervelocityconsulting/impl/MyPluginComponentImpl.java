package com.hypervelocityconsulting.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.hypervelocityconsulting.api.MyPluginComponent;

import javax.inject.Named;

@ExportAsService ({MyPluginComponent.class})
@Named ("myPluginComponent")
public class MyPluginComponentImpl implements MyPluginComponent
{

    public String getName()
    {

        return "myComponent";
    }
}