package com.hypervelocityconsulting.job;

/**
 * Created by natalia on 22.08.17.
 */
public interface Monitor {
    public void reschedule();
}
