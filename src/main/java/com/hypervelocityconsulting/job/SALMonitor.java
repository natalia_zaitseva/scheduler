package com.hypervelocityconsulting.job;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.*;
import com.atlassian.scheduler.config.*;
import com.atlassian.scheduler.status.JobDetails;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ExportAsService({Monitor.class})
@Named("SALMonitor")
public class SALMonitor implements Monitor, InitializingBean, DisposableBean, LifecycleAware {

    private final JobRunnerKey JOB_NAME = JobRunnerKey.of(SALMonitor.class.getName());
    private final String JOB_ID = SALMonitor.class.getName() + ":job";
    private final Logger logger = Logger.getLogger(SALMonitor.class);
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final SchedulerService schedulerService;
    private final CustomFieldManager customFieldManager;
    private final EventPublisher eventPublisher;
    private final SearchService searchService;
    private final IssueManager issueManager;
    private final IssueIndexingService issueIndexingService;

    @Inject
    public SALMonitor(@ComponentImport JiraAuthenticationContext jiraAuthenticationContext,
                      @ComponentImport CustomFieldManager customFieldManager,
                      @ComponentImport SchedulerService schedulerService,
                      @ComponentImport EventPublisher eventPublisher,
                      @ComponentImport SearchService searchService,
                      @ComponentImport IssueManager issueManager,
                      @ComponentImport IssueIndexingService issueIndexingService) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.customFieldManager = customFieldManager;
        this.schedulerService = schedulerService;
        this.eventPublisher = eventPublisher;
        this.searchService = searchService;
        this.issueManager = issueManager;
        this.issueIndexingService = issueIndexingService;
    }

    public void reschedule() {
        JobDetails jobDetails = schedulerService.getJobDetails(JobId.of(JOB_ID));

        if (jobDetails == null) {

            Map<String, Serializable> parameters = new HashMap<String, Serializable>();
            parameters.put(JOB_ID, 123);
            try {
                schedulerService.scheduleJob(JobId.of(JOB_ID),
                        JobConfig.forJobRunnerKey(JOB_NAME)
                                .withRunMode(RunMode.RUN_ONCE_PER_CLUSTER)
                                .withParameters(parameters)
                                .withSchedule(Schedule.forInterval(30000L, new Date(System.currentTimeMillis()))));
            } catch (SchedulerServiceException e) {
                e.printStackTrace();
            }
        }
    }

    public void destroy() throws Exception {
        eventPublisher.unregister(this);
        schedulerService.unscheduleJob(JobId.of(JOB_ID));
    }

    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
        schedulerService.registerJobRunner(JOB_NAME, new JobRunner() {
            @Nullable
            public JobRunnerResponse runJob(JobRunnerRequest jobRunnerRequest) {
                logger.info("Start the job");

                ApplicationUser loggedInUser = jiraAuthenticationContext.getLoggedInUser();
                JqlQueryBuilder jqlQueryBuilder = JqlQueryBuilder.newBuilder();
                jqlQueryBuilder.where().customField(10600L).isNotEmpty(); // id of the Target End custom field
                JqlClauseBuilder jqlClauseBuilder = JqlQueryBuilder.newClauseBuilder(jqlQueryBuilder.buildQuery());
                Query query = jqlClauseBuilder.buildQuery();

                try {
                    SearchResults searchResults = searchService.searchOverrideSecurity(loggedInUser, query, PagerFilter.getUnlimitedFilter());
                    List<Issue> allIssues = searchResults.getIssues();
                    logger.info("amount of the issue to process: " + allIssues.size());
                    CustomField sourceDateField = customFieldManager.getCustomFieldObjectByName("Target End"); // Target End custom field
                    CustomField targetDateField = customFieldManager.getCustomFieldObjectByName("Target Date"); // Target Date custom Field
                    if (targetDateField != null && sourceDateField != null ) {
                        logger.info("both not null");
                        for (Issue issue : allIssues) {
                            logger.info("current Issue " + issue.getKey());
                            Date targetEndValue = (Date)issue.getCustomFieldValue(sourceDateField);
                            logger.info("targetEndValue " + targetEndValue);
                            if (targetEndValue != null) {
                                logger.info("NOT NULL");
                                Timestamp targetEndTimestampValue = new Timestamp(targetEndValue.getTime());
                                Timestamp targetDateTimestampValue = (Timestamp) issue.getCustomFieldValue(targetDateField);
                                if (targetDateTimestampValue == null || targetEndValue.after(targetDateTimestampValue)) {
                                    ModifiedValue modifiedValue = new ModifiedValue(issue.getDueDate(), targetEndTimestampValue);
                                    targetDateField.updateValue(null, issue, modifiedValue, new DefaultIssueChangeHolder());
                                }
                            }
                        }
                    }
                } catch (SearchException e) {
                    e.printStackTrace();
                }

                return JobRunnerResponse.success();
            }
        });

    }

    @EventListener
    public void onPluginEnabled(PluginEnabledEvent event) {
        eventPublisher.unregister(this);
        reschedule();
    }

    public void onStart() {
    }

    public void onStop() {
    }
}
